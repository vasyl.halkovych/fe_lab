const http = require('http');

const PORT = 8080;

http.createServer((request, response) => {
    console.log((`${request.method} ${request.url}`));
    response.end('hello Node.js');
}).listen(PORT);